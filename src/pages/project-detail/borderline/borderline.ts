import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';


@Component({
  selector: 'page-borderline',
  templateUrl: 'borderline.html'
})
export class BorderlinePage {

  constructor(public navCtrl: NavController) {

  }
}

import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { BorderlinePage } from '../project-detail/borderline/borderline';
import { GeocatchingPage } from '../project-detail/geocatching/geocatching';
import { GeotweetPage } from '../project-detail/geotweet/geotweet';
import { MimirPage } from '../project-detail/mimir/mimir';

@Component({
  selector: 'page-project',
  templateUrl: 'project.html'
})
export class ProjectPage {

  constructor(public navCtrl: NavController) {

  }

  openPage(page){ 
  	switch (page) {
  		case "BorderlinePage":
  				this.navCtrl.push(BorderlinePage);
  			break;
  		case "GeocatchingPage":
  				this.navCtrl.push(GeocatchingPage);
  			break;
  		case "GeotweetPage":
  				this.navCtrl.push(GeotweetPage);
  			break;
  		case "MimirPage":
  				this.navCtrl.push(MimirPage);
  			break;
  	}
  } 
}

import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';


@Component({
  selector: 'page-geocatching',
  templateUrl: 'geocatching.html'
})
export class GeocatchingPage {

  constructor(public navCtrl: NavController) {

  }
}

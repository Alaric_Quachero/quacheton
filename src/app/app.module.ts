import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { TranslateModule, TranslateStaticLoader, TranslateLoader } from 'ng2-translate/ng2-translate';
import { Http } from '@angular/http';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { ProjectPage } from '../pages/project/project';
import { BorderlinePage } from '../pages/project-detail/borderline/borderline';
import { GeocatchingPage } from '../pages/project-detail/geocatching/geocatching';
import { GeotweetPage } from '../pages/project-detail/geotweet/geotweet';
import { MimirPage } from '../pages/project-detail/mimir/mimir';
import { TeamPage } from '../pages/team/team';
import { FabienBlanchetonPage } from '../pages/team-details/fabien-blancheton/fabien-blancheton';
import { AlaricQuacheroPage } from '../pages/team-details/alaric-quachero/alaric-quachero';
import { ContactPage } from '../pages/contact/contact';
import { ContactFormPage } from '../pages/contact-form/contact-form';
import { SettingPage } from '../pages/setting/setting';

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, 'assets/i18n', '.json');
}
@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    HomePage,
    ProjectPage,
    BorderlinePage,
    GeocatchingPage,
    GeotweetPage,
    MimirPage,
    TeamPage,
    FabienBlanchetonPage,
    AlaricQuacheroPage,
    ContactPage,
    ContactFormPage,
    SettingPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
    provide: TranslateLoader,
    useFactory: (createTranslateLoader),
    deps: [Http]
  })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    HomePage,
    ProjectPage,
    BorderlinePage,
    GeocatchingPage,
    GeotweetPage,
    MimirPage,
    TeamPage,
    FabienBlanchetonPage,
    AlaricQuacheroPage,
    ContactPage,
    ContactFormPage,
    SettingPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}

import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { ModalController } from 'ionic-angular';

import { ContactFormPage } from '../contact-form/contact-form';

import {
 GoogleMap,
 GoogleMapsEvent,
 GoogleMapsLatLng,
 CameraPosition,
 GoogleMapsMarkerOptions,
 GoogleMapsMarker,
} from 'ionic-native';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

  }

    // Load map only after view is initialize
	ngAfterViewInit() {
		this.loadMap();
	}

	loadMap() {

	 // create a new map by passing HTMLElement
	 let element: HTMLElement = document.getElementById('map');

	 let map = new GoogleMap(element);

	 // create LatLng object
	 let ionic: GoogleMapsLatLng = new GoogleMapsLatLng(43.61591575058267, 7.07281923154369);

	 // create CameraPosition
	 let position: CameraPosition = {
	   target: ionic,
	   zoom: 18,
	   tilt: 30
	 };

	 // listen to MAP_READY event
	 map.one(GoogleMapsEvent.MAP_READY).then(() => {
	   // move the map's camera to position
	   map.moveCamera(position); // works on iOS and Android
	}).catch(err => console.log(err));


	 // create new marker
	 let markerOptions: GoogleMapsMarkerOptions = {
	   position: ionic,
	   title: 'Ionic'
	 };

	 map.addMarker(markerOptions)
	   .then((marker: GoogleMapsMarker) => {
	      marker.showInfoWindow();
	    }).catch(err => console.log(err));
	}

	openModal() {
	    let modal = this.modalCtrl.create(ContactFormPage);
	    modal.present();
  	}
}


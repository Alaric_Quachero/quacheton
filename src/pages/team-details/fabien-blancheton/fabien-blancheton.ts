import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { EmailComposer } from 'ionic-native';

@Component({
  selector: 'page-fabien-blancheton',
  templateUrl: 'fabien-blancheton.html'
})
export class FabienBlanchetonPage {

  constructor(public navCtrl: NavController) {

  }

	mail = {
		subject: [''],
		message: ['']
	}

	formMail() {
	let email = {
		to: 'blancheton.fabien@gmail.com',
		subject: this.mail.subject.toString(),
		body: this.mail.message.toString()
	};
	EmailComposer.open(email);
	}
}
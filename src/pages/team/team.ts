import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { FabienBlanchetonPage } from '../team-details/fabien-blancheton/fabien-blancheton';
import { AlaricQuacheroPage } from '../team-details/alaric-quachero/alaric-quachero';


@Component({
  selector: 'page-team',
  templateUrl: 'team.html'
})
export class TeamPage {
  constructor(public navCtrl: NavController) {

  }

  detail(choix) {
    switch (choix) {
      case 'Fabien':
        this.navCtrl.push(FabienBlanchetonPage);
        break;
      case 'Alaric':
        this.navCtrl.push(AlaricQuacheroPage);
        break;
      default:
        // code...
        break;
    }
  }
}

import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';


@Component({
  selector: 'page-mimir',
  templateUrl: 'mimir.html'
})
export class MimirPage {

  constructor(public navCtrl: NavController) {

  }
}
